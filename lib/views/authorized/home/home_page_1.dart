import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../controllers/home/home_1_controller.dart';
import '../../../routes/app_path.dart';

class HomePage1 extends StatelessWidget {
  HomePage1({super.key});
  final HomePage1Controller _controllerA = Get.put(HomePage1Controller());
  @override
  Widget build(BuildContext context) {
    print(Get.arguments);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home page 1'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Obx(() => Text(_controllerA.count.value.toString())),
          ElevatedButton(
            onPressed: () {
              _controllerA.nextCount();
            },
            child: const Text('next count'),
          ),
          const SizedBox(
            height: 20,
          ),
          GetBuilder<HomePage1Controller>(
            id: 1,
            builder: (controller) => Text(controller.count.toString()),
          ),
          GetBuilder<HomePage1Controller>(
            id: 2,
            builder: (controller) => Text(controller.count.toString()),
          ),
          Center(
            child: ElevatedButton(
              onPressed: () async {
                var demo = await Get.toNamed(Routes.homePath2);
                print(demo);
              },
              child: const Text('next page 2'),
            ),
          ),
        ],
      ),
    );
  }
}
