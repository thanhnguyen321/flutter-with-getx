import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../controllers/home/home_2_controller.dart';
import '../../../controllers/home/home_3_controller.dart';
import '../../../routes/app_path.dart';

class HomePage3 extends StatelessWidget {
  const HomePage3({super.key});

  // static final Home3Controller he = Get.put(Home3Controller());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home page 3'),
      ),
      body: Column(
        children: [
          // Text('${abc.count}'),
          Center(
            child: ElevatedButton(
              onPressed: () {
                Get.toNamed(Routes.homePath4);
              },
              child: const Text('next page 4'),
            ),
          ),
        ],
      ),
    );
  }
}
