import 'package:app_get_x/routes/app_path.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../controllers/home/home_2_controller.dart';

class HomePage2 extends StatelessWidget {
  const HomePage2({super.key});
  static final abc = Get.put(HomePage2Controller());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('HOme page 2'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(abc.name),
          ElevatedButton(
            onPressed: () {
              Get.back(result: 'back back');
            },
            child: const Text('back'),
          ),
          Center(
            child: ElevatedButton(
              onPressed: () {
                Get.toNamed(Routes.homePath3);
              },
              child: const Text('next page 3'),
            ),
          ),
        ],
      ),
    );
  }
}
