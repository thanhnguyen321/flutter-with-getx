import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../routes/app_path.dart';

class HomePage4 extends StatelessWidget {
  const HomePage4({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home page 4'),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            Get.back();
          },
          child: const Text('back to page'),
        ),
      ),
    );
  }
}
