import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ProfilePage1 extends StatelessWidget {
  const ProfilePage1({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Profile page 1'),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            Get.toNamed('/profile/profile2');
          },
          child: const Text('next page 2'),
        ),
      ),
    );
  }
}
