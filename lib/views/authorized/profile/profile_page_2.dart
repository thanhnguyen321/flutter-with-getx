import 'package:flutter/material.dart';

class ProfilePage2 extends StatelessWidget {
  const ProfilePage2({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Profile 2'),
      ),
      body: const Center(
        child: Text('Profile page 2'),
      ),
    );
  }
}
