import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../routes/app_path.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text(
              'LOgIn',
              style: TextStyle(fontSize: 24),
            ),
            const SizedBox(height: 20),
            ElevatedButton(
              onPressed: () {
                Get.toNamed(Routes.home);
              },
              child: const Text('LOgIn'),
            ),
          ],
        ),
      ),
    );
  }
}
