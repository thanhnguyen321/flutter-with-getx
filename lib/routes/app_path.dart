class Routes {
  static String login = '/login';
  static String home = '/home';
  static String home1 = '/home1';
  static String homePath1 = '$home/home1';
  static String home2 = '/home2';
  static String homePath2 = '$home/home2';
  static String home3 = '/home3';
  static String homePath3 = '$homePath2/home3';
  static String home4 = '/home4';
  static String homePath4 = '$homePath3/home4';
  static String profile = '/profile';
  static String profile1 = '/profile1';
  static String profile2 = '/profile2';

  static String notFound = '/not-found';
}
