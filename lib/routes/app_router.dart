import 'package:app_get_x/views/authorized/home/home_page.dart';
import 'package:app_get_x/views/authorized/home/home_page_2.dart';
import 'package:app_get_x/views/authorized/home/home_page_3.dart';
import 'package:app_get_x/views/authorized/home/home_page_4.dart';
import 'package:app_get_x/views/authorized/profile/profile_page.dart';
import 'package:app_get_x/views/authorized/profile/profile_page_1.dart';
import 'package:app_get_x/views/authorized/profile/profile_page_2.dart';
import 'package:app_get_x/views/splash.dart';
import 'package:app_get_x/views/un_authorized/login_page.dart';
import 'package:get/route_manager.dart';

import '../views/authorized/home/home_page_1.dart';
import 'app_path.dart';
import 'bindings/authorized/home_2_binding.dart';

class AppRouterPages {
  static String initial = Routes.notFound;
  static final notFound =
      GetPage(name: Routes.notFound, page: () => const Splash());

  /// unAuthorized
  static List<GetPage> unAuthPages = [
    GetPage(
      name: Routes.login,
      page: () => const LoginPage(),
    ),
  ];

  /// authorized
  static List<GetPage> authPages = [
    GetPage(
      name: Routes.home,
      page: () => const HomePage(),
      children: [
        GetPage(
          name: Routes.home1,
          page: () => HomePage1(),
        ),
        GetPage(
          name: Routes.home2,
          page: () => const HomePage2(),
          binding: HomePage2Binding(),
          children: [
            GetPage(
                name: Routes.home3,
                page: () => const HomePage3(),
                children: [
                  GetPage(
                    name: Routes.home4,
                    page: () => const HomePage4(),
                  ),
                ]),
          ],
        ),
      ],
    ),
    GetPage(name: Routes.profile, page: () => const ProfilePage(), children: [
      GetPage(
        name: Routes.profile1,
        page: () => const ProfilePage1(),
      ),
      GetPage(
        name: Routes.profile2,
        page: () => const ProfilePage2(),
      ),
    ]),
  ];

  static List<GetPage> getPage = [
    ...authPages,
    ...unAuthPages,
  ];
}
