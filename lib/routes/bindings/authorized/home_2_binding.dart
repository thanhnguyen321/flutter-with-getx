import 'package:app_get_x/controllers/home/home_3_controller.dart';
import 'package:get/get.dart';

import '../../../controllers/home/home_1_controller.dart';
import '../../../controllers/home/home_2_controller.dart';

class HomePage2Binding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomePage2Controller>(() => HomePage2Controller());

    /// bindings nhieu controllers
    // Get
    //   ..lazyPut<HomePage2Controller>(() => HomePage2Controller())
    //   ..put(HomePage1Controller())
    //   ..put(Home3Controller());
  }
}
