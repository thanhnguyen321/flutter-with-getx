import 'package:get/get.dart';

import '../base_controller.dart';

class HomePage2Controller extends BaseController {
  final RxString _name = 'thanh'.obs;
  String get name => _name.value;
  final RxInt _count = 0.obs;
  int get count => _count.value;

  void nextCount() {
    _count.value++;
    update();
  }
}
