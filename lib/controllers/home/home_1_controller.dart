import 'package:get/get.dart';

import '../base_controller.dart';

class HomePage1Controller extends BaseController {
  RxString name = 'thanh'.obs;
  RxInt count = 0.obs;

  void nextCount() {
    print('count');
    count.value++;
    update([1]);
  }

  @override
  void onInit() {
    print('demo: onInit');
    super.onInit();
  }

  @override
  void onClose() {
    print('demo: onClose');
    super.onClose();
  }

  @override
  void onReady() {
    print('demo: onReady');
    super.onReady();
  }
}
