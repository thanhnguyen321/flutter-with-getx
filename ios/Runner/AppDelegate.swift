import UIKit
import Flutter


@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
    
    UNUserNotificationCenter.current()
    .requestAuthorization(options: [.alert, .sound]) {(granted, error) in
        // Make sure permission to receive push notifications is granted
        print("Permission is granted: \(granted)")
}
    GeneratedPluginRegistrant.register(with: self)
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
}
